const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const cssnano = require("cssnano");

module.exports = {
  entry: ["babel-polyfill", path.resolve(__dirname, "../src/app.jsx")],
  output: {
    path: path.resolve(__dirname, "../dist"),
    publicPath: "/",
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: "html-loader",
        exclude: path.resolve(__dirname, "node_modules"),
        query: {
          minimize: true
        }
      },
      {
        test: /\.(js|jsx)$/,
        exclude: [path.resolve(__dirname, "node_modules"), path.resolve(__dirname, "dist"), path.resolve(__dirname, "webpack")],
        use: [
          {
            loader: "babel-loader",
            query: { compact: false }
          }
        ]
      },
      {
        test: /\.css$/,
        exclude: path.resolve(__dirname, "node_modules"),
        use: [
          ExtractTextPlugin.loader,
          {
            loader: "css-loader"
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: path.resolve(__dirname, "node_modules"),
        use: [
          ExtractTextPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: true,
              sourceMap: true,
              importLoaders: 1,
              localIdentName: "[name]__[local]___[hash:base64:5]"
            }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: () => [cssnano()]
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/"
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "svg-inline-loader",
            options: {
              removeSVGTagAttrs: false
            }
          }
        ]
      }
    ]
  },
  resolve: {
    modules: ["node_modules", "src"],
    extensions: ["index.js", "index.jsx", ".js", ".jsx", ".json", ".scss"],
    alias: {
      "@routes": path.resolve(__dirname, "../src/routes/"),
      "@services": path.resolve(__dirname, "../src/services/")
    }
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "../public/index.html")
    }),
    new ExtractTextPlugin({
      filename: "[name].css"
    })
  ]
};
