// @flow

import * as types from "./types";

type State = {
  people: Array<any>,
  userSelected?: Object<any>
};

const initialState: State = {
  people: [],
  userSelected: null
};

export default function people(state: State = initialState, action: Object): State {
  let result = { ...state };
  switch (action.type) {
    case types.GET_PEOPLE + "_REQUEST": {
      result.people = [];
      break;
    }
    case types.GET_PEOPLE + "_SUCCESS": {
      result.people = action.result.data.results;
      result.userSelected = action.result.data.results[0];
      break;
    }
    case types.GET_PEOPLE + "_FAILURE": {
      result.people = [];
      break;
    }

    case types.SELECT_USER: {
      result.userSelected = action.user;
      break;
    }

    default:
      return result;
  }
  return result;
}
