import * as types from "./types";

import axios from "axios";

export function getPeopel() {
  return {
    type: types.GET_PEOPLE,
    promise: axios.get("https://swapi.co/api/people")
  };
}

export function selectUser(user: Object<any>) {
  return {
    type: types.SELECT_USER,
    user
  };
}
