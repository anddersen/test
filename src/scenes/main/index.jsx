// @flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import classNames from "classnames";
import update from "immutability-helper";
import { Grid, InputBase, FormControl, FormLabel, Radio, RadioGroup, FormControlLabel } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

import Header from "../../components/header/index.jsx";
import Item from "./components/item/index.jsx";
import ListPeople from "./components/listPeople/index.jsx";

import * as actionsPeople from "../../services/people/actions";

import styles from "./style.scss";

interface PropsType {
  people: Array<any>;
  userSelected?: Object<any>;

  getPeopel(): Promise<any>;
  selectUser(user: Object<any>): void;
}

interface StateType {
  req: {
    value: string,
    gender: string,
    eye_color: string
  };
}

class Main extends Component<PropsType, StateType> {
  constructor(props: any) {
    super(props);
    this.state = {
      req: {
        value: "",
        gender: "off",
        eye_color: "off"
      }
    };
  }

  componentDidMount() {
    this.props.getPeopel();
  }

  handleChange(e) {
    const value = e.target.value;
    let state = update(this.state, {
      req: {
        value: { $set: value }
      }
    });
    this.setState(state);
  }

  handleChangeFilter(e) {
    const value = e.target.value;
    const name = e.target.name;

    let state = update(this.state, {
      req: {
        [name]: { $set: value }
      }
    });
    this.setState(state);
  }

  render() {
    const { people, userSelected, selectUser } = this.props;
    const { req } = this.state;

    let listPeaple = people;

    listPeaple = people.filter(user => {
      return user.name.indexOf(req.value) !== -1;
    });

    if (req.gender !== "off") {
      listPeaple = listPeaple.filter(user => {
        return user.gender === req.gender;
      });
    }

    if (req.eye_color !== "off") {
      listPeaple = listPeaple.filter(user => {
        return user.eye_color === req.eye_color;
      });
    }

    return (
      <Fragment>
        <Header>
          <div className={classNames(styles.search)}>
            <div className={classNames(styles.search_icon)}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classNames(styles.search_root)
              }}
              value={req.value}
              onChange={e => this.handleChange(e)}
            />
          </div>
        </Header>

        <div className={classNames(styles.root)}>
          <Grid container spacing={24}>
            <Grid item xs={6} sm={4}>
              <FormControl component="fieldset">
                <FormLabel component="legend">Gender</FormLabel>
                <RadioGroup aria-label="Gender" name="gender" value={req.gender} onChange={e => this.handleChangeFilter(e)}>
                  <FormControlLabel value="female" control={<Radio />} label="Female" />
                  <FormControlLabel value="male" control={<Radio />} label="Male" />
                  <FormControlLabel value="n/a" control={<Radio />} label="n/a" />
                  <FormControlLabel value="off" control={<Radio />} label="off" />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={6} sm={4}>
              <FormControl component="fieldset">
                <FormLabel component="legend">Eye color</FormLabel>
                <RadioGroup aria-label="eye_color" name="eye_color" value={req.eye_color} onChange={e => this.handleChangeFilter(e)}>
                  <FormControlLabel value="blue" control={<Radio />} label="blue" />
                  <FormControlLabel value="yellow" control={<Radio />} label="yellow" />
                  <FormControlLabel value="red" control={<Radio />} label="red" />
                  <FormControlLabel value="brown" control={<Radio />} label="brown" />
                  <FormControlLabel value="blue-gray" control={<Radio />} label="blue-gray" />
                  <FormControlLabel value="off" control={<Radio />} label="off" />
                </RadioGroup>
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
              <ListPeople people={listPeaple} userSelected={userSelected} selectUser={selectUser} />
            </Grid>
            <Grid item xs={12} sm={6}>
              {userSelected ? <Item userSelected={userSelected} /> : null}
            </Grid>
          </Grid>
        </div>
      </Fragment>
    );
  }
}

export default connect(
  state => ({
    people: state.people.people,
    userSelected: state.people.userSelected
  }),
  { getPeopel: actionsPeople.getPeopel, selectUser: actionsPeople.selectUser }
)(Main);
