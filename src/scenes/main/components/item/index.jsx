// @flow
import React, { PureComponent } from "react";
import { Card, CardContent, Typography } from "@material-ui/core";

interface PropsType {
  userSelected?: Object<any>;
}

export default class Item extends PureComponent<PropsType> {
  constructor(props: any) {
    super(props);
  }

  render() {
    const { userSelected } = this.props;

    return (
      <Card>
        <CardContent>
          <Typography variant="h5" component="h2">
            {userSelected.name}
          </Typography>
          <Typography paragraph color="textSecondary">
            {userSelected.birth_year}
          </Typography>
          <Typography paragraph>
            <b>Eye color</b>: {userSelected.eye_color}
          </Typography>
          <Typography paragraph>
            <b>gender</b>: {userSelected.gender}
          </Typography>
          <Typography paragraph>
            <b>Hair color</b>: {userSelected.hair_color}
          </Typography>
          <Typography paragraph>
            <b>homeworld</b>:{" "}
            <a href={userSelected.homeworld} target="_blank">
              {userSelected.homeworld}
            </a>
          </Typography>
          <Typography paragraph>
            <b>mass</b>: {userSelected.mass}
          </Typography>
          <Typography paragraph>
            <b>Skin color</b>: {userSelected.skin_color}
          </Typography>
        </CardContent>
      </Card>
    );
  }
}
