// @flow
import React, { Fragment, PureComponent } from "react";
import { List, ListItem, ListItemText, Divider } from "@material-ui/core";

interface PropsType {
  people: Array<any>;
  userSelected?: Object<any>;

  selectUser(user: Object<any>): void;
}

export default class ListPeople extends PureComponent<PropsType> {
  constructor(props: any) {
    super(props);
  }

  changeUser(user: Object<any>) {
    const { selectUser } = this.props;

    selectUser(user);
  }

  render() {
    const { people, userSelected } = this.props;

    return (
      <List component="nav">
        {people.map((user, i) => {
          return (
            <Fragment key={i}>
              <ListItem button selected={userSelected.name === user.name} onClick={() => this.changeUser(user)}>
                <ListItemText inset primary={user.name} />
              </ListItem>
              <Divider />
            </Fragment>
          );
        })}
      </List>
    );
  }
}
