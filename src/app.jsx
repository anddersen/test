/* eslint-env browser */

import React, { Component } from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { Provider } from "react-redux";

import Main from "./scenes/main/index.jsx";

import reducers from "./services/index";

import promiseMiddleware from "./middleware/promiseMiddleware";

const enhancer = applyMiddleware(promiseMiddleware);
const store = createStore(reducers, enhancer);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
