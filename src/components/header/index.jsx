// @flow
import React, { PureComponent } from "react";

import { AppBar, Toolbar, Typography } from "@material-ui/core";

export default class Header extends PureComponent {
  render() {
    return (
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            Films
          </Typography>
          {this.props.children}
        </Toolbar>
      </AppBar>
    );
  }
}
