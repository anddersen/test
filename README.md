# React Redux Boilerplate

## Основные пакеты

1. Экосистема React: `react`, `redux`, `react-router`
1. Типизация: `flow`
1. Асинхроннсть: `axios`, `redux-saga`
1. Тесты: `jest`, `enzyme`
1. Линтеры: `eslint, stylelint`
1. Форматирование кода: `prettier`

## Установка и запуск

1. `npm install`
2. `npm start`
